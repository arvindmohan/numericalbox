#ifndef LINEARSOLVERS_H_INCLUDED
#define LINEARSOLVERS_H_INCLUDED

class linearsolvers
{
  private:
  int m,n,mat_size;
  double tol;
  public:
  std::vector<double> gauss_seidel(const std::vector< std::vector<double> >&,const std::vector<double>&, const double&);
};

//********************Function Gauss-Seidel

std::vector<double> linearsolvers::gauss_seidel(const std::vector< std::vector<double> >& a,const std::vector<double>& b,const double& init)
{
   mat_size = a.size();
   std::cout << "mat size"<< mat_size << "\n";
   m = mat_size; n = mat_size;
   std::vector<double> x(m,init);
   std::cout << "n size"<< n << "\n";
   std::cout << "m size"<< m << "\n";
   std::vector<double> xn(m,0);
   std::vector<double> xo(m,0);
   //std::vector<double> xo(m,0);
   double term1 = 0;
   double term2 = 0;
   tol = 1e-06;
   //std::vector<double> error1(m,0);
   double error = 1.0;
   int iter = 0;

  while (error > tol)
  {
      term1 = 0; term2 = 0;
      for (int i=0;i<m;i++)
      {
          std::cout << "\n" << "The current val is" << "\n";
          print_array1d(x);
          // At 1st Row
      if (i==0)
      {
          std::cout<<"start1" << "\n";
          term2=0;
          for(int j=1;j<n;j++)
          {
              std::cout << "ur here0"<< "\n";
              term2 = term2 + (a[i][j]*x[j]);
              std::cout << "xo value is" << x[j] << "\n";
          }

          x[i] = (1.0/a[i][i])*(b[i] - term2);
          std::cout << "x0 first is " << x[0] << "\n";
      }
       // At last row
      if (i==(m-1))
      {
          term1=0;
          for(int j=0;j<i;j++)
          {
           std::cout << "ur here1"<< "\n";
           term1 = term1 + (a[i][j]*x[j]);
          }

          x[i] = (1.0/a[i][i])*(b[i] - term1);
          std::cout << "x0 last is " << x[i] << "\n";
      }
      term1=0;
      term2=0;
          for(int j=0;j<i;j++)
          {
              std::cout << "ur here2"<< "\n";
              term1 = term1 + (a[i][j]*x[j]);
          }

          for(int j=i+1;j<n;j++)
          {
              std::cout << "ur here3"<< "\n";
              term2 = term2 + (a[i][j]*x[j]);
          }

          x[i] = (1.0/a[i][i])*(b[i] - term1 - term2);
          std::cout << "x0 inter is " << x[i] << "\n";
     }
          std::cout << "\n" << "The old val is" << "\n";
          print_array1d(xo);
          error = maxval(mod(diff(x,xo)));
          xo = x;
    iter = iter+1;
    std::cout << "iter no. is" << iter << "\n" << "Error is" << error << "\n";
   }
    return(x);
}


#endif // LINEARSOLVERS_H_INCLUDED
